import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import InitLayout from './src/Layouts/InitLayout/InitLayout.js';
import FlashMessage from 'react-native-flash-message';
import { Provider } from 'react-redux';
import store from './src/store/';

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Provider store={store}>
          <InitLayout />
          <FlashMessage position="top" />
        </Provider>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center'
  }
});
