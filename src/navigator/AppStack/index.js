import { createStackNavigator, createAppContainer } from 'react-navigation';
import DashboardScreen from '$screens/DashboardScreen';
import WeekWeatherScreen from '$screens/WeekWeatherScreen';
import { Dashboard, WeekWeather } from './constants';

import { config } from './config';

export const AppStack = createAppContainer(
  createStackNavigator(
    {
      [Dashboard]: DashboardScreen,
      [WeekWeather]: WeekWeatherScreen
    },
    config
  )
);
