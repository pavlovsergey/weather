import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import InitScreen from '$screens/InitScreen';
import AppLayout from '$Layouts/AppLayout';
import { App, InitLoading } from './constants';

export default createAppContainer(
  createSwitchNavigator(
    {
      [InitLoading]: InitScreen,
      [App]: AppLayout
    },
    {
      initialRouteName: InitLoading
    }
  )
);
