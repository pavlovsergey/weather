import { GET_POSITION_USER } from '$store/constants/user';
import { SET_OPEN_PLACE } from '$store/constants/place';

const stateDefault = {
  coords: { latitude: 50.4501, longitude: 30.5234 },
  isRefreshPosition: false
};

const location = (stateLocation = stateDefault, { type, payload }) => {
  switch (type) {
    case GET_POSITION_USER:
      return {
        ...stateLocation,
        ...payload.data,
        isRefreshPosition: true
      };
    case SET_OPEN_PLACE:
      return {
        ...stateLocation,
        latitude: payload.latitude,
        longitude: payload.longitude
      };

    default:
      return stateLocation;
  }
};

export default location;
