import {
  CLEAR_NOTIFICATION,
  NOTIFICATION
} from '$store/constants/notification';

const stateNotificationDefault = {
  isNotification: false,
  notificationMessage: '',
  typeNotification: 'info',
  description: ''
};

const notification = (
  stateNotification = stateNotificationDefault,
  { type, payload }
) => {
  switch (type) {
    case NOTIFICATION:
      return {
        isNotification: true,
        notificationMessage: payload.notificationMessage,
        typeNotification: payload.typeNotification,
        description: payload.description
      };

    case CLEAR_NOTIFICATION:
      return {
        isNotification: false,
        notificationMessage: '',
        typeNotification: 'info',
        description: ''
      };

    default:
      return stateNotification;
  }
};

export default notification;
