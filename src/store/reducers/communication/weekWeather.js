import { setLoadingTrue, setLoadTrue, setLoadFail } from './utils';
import {
  FAIL_LOAD_WEEK_WEATHER,
  LOADING_WEEK_WEATHER,
  LOAD_WEEK_WEATHER
} from '$store/constants/place';

const stateDefault = {
  isLoading: false,
  isLoad: false,
  isErrorLoad: false,
  error: { message: '' }
};

const weekWeather = (state = stateDefault, { type, payload }) => {
  switch (type) {
    case LOADING_WEEK_WEATHER:
      return {
        ...setLoadingTrue(state)
      };

    case LOAD_WEEK_WEATHER:
      return {
        ...setLoadTrue(state)
      };

    case FAIL_LOAD_WEEK_WEATHER:
      return {
        ...setLoadFail(state, payload)
      };
    default:
      return state;
  }
};

export default weekWeather;
