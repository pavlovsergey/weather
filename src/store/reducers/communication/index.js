import { combineReducers } from 'redux';
import weekWeather from './weekWeather';

export default combineReducers({
  weekWeather
});
