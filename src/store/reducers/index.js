import { combineReducers } from 'redux';
import app from './app';
import permissions from './permissions';
import user from './user';
import notification from './notification';
import mapMarker from './mapMarker';
import communication from './communication';
import place from './place';

export default combineReducers({
  app,
  permissions,
  user,
  notification,
  mapMarker,
  place,
  communication
});
