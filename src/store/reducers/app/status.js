import { STATUS_APP } from '$store/constants/app';
import { InitLoading, App } from '$navigator/InitStack/constants';

const stateDefault = InitLoading;

const status = (state = stateDefault, { type }) => {
  switch (type) {
    case STATUS_APP:
      return App;

    default:
      return state;
  }
};

export default status;
