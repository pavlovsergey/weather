import { LOAD_WEEK_WEATHER } from '$store/constants/place';
const stateDefault = {
  country: '',
  postalCode: '',
  isoCountryCode: '',
  name: '',
  city: '',
  street: '',
  region: ''
};

const address = (state = stateDefault, { type, payload }) => {
  switch (type) {
    case LOAD_WEEK_WEATHER:
      if (!payload) return state;
      return payload.address;

    default:
      return state;
  }
};

export default address;
