import { combineReducers } from 'redux';
import coordinate from './coordinate';
import weather from './weather';
import address from './address';

export default combineReducers({
  coordinate,
  address,
  weather
});
