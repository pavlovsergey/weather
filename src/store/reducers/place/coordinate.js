import { SET_OPEN_PLACE } from '$store/constants/place';

const stateDefault = { latitude: 50.4501, longitude: 30.5234 };

const coordinate = (state = stateDefault, { type, payload }) => {
  switch (type) {
    case SET_OPEN_PLACE:
      return {
        ...state,
        latitude: payload.latitude,
        longitude: payload.longitude
      };

    default:
      return state;
  }
};

export default coordinate;
