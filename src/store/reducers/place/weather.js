import { LOAD_WEEK_WEATHER } from '$store/constants/place';
const stateDefault = [];

const weather = (state = stateDefault, { type, payload }) => {
  switch (type) {
    case LOAD_WEEK_WEATHER:
      if (!payload) return state;
      return payload.weather;

    default:
      return state;
  }
};

export default weather;
