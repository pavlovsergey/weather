import { PERMISSIONS } from '$store/constants/permissions';

const stateDefault = false;

const permissions = (statePermissions = stateDefault, { type }) => {
  switch (type) {
    case PERMISSIONS.LOCATION:
      return true;

    default:
      return statePermissions;
  }
};

export default permissions;
