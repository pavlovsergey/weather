export const checkExist = (arr, item) => {
  let isExist = false;

  const newArr = arr.map(itemExist => {
    if (itemExist.city_name === item.city_name) {
      isExist = true;
      return item;
    }
    return itemExist;
  });

  if (isExist) {
    return newArr;
  }

  return [...newArr, item];
};
