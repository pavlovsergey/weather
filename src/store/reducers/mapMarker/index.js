import { SET_MAP_MARKER } from '$store/constants/mapMarker';
import { checkExist } from './utils';
const stateDefault = [];

const mapMarker = (state = stateDefault, { type, payload }) => {
  switch (type) {
    case SET_MAP_MARKER:
      return checkExist(state, payload.data);

    default:
      return state;
  }
};

export default mapMarker;
