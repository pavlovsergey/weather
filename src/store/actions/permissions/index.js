import { PERMISSIONS } from '$store/constants/permissions';
import { Permissions } from 'expo';
import { handlerError } from '../error';

export const getPermissions = type => {
  return async dispatch => {
    const { status } = await Permissions.getAsync(Permissions[type]);

    if (status !== 'granted') return dispatch(askPermission(type));
    return dispatch({ type: PERMISSIONS[type] });
  };
};

export const askPermission = type => {
  return async dispatch => {
    try {
      const { status } = await Permissions.askAsync(Permissions[type]);
      if (status === 'denied') return;
      return dispatch({ type: PERMISSIONS[type] });
    } catch (e) {
      handlerError(dispatch, e);
    }
  };
};
