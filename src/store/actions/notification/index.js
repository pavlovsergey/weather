import {
  NOTIFICATION,
  CLEAR_NOTIFICATION
} from '$store/constants/notification';

export const notification = ({ name, description, type }) => {
  return {
    type: NOTIFICATION,

    payload: {
      notificationMessage: name,
      description: description,
      typeNotification: type
    }
  };
};
export const clearNotification = () => {
  return { type: CLEAR_NOTIFICATION };
};
