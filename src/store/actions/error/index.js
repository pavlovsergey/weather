import { NOTIFICATION } from '$store/constants/notification';

export const handlerError = (dispatch, e, type) => {
  const name = e.name || 'error';
  const message = e.message || '';
  if (type) {
    dispatch({
      type,
      payload: {
        message: `${name} ${message}`
      }
    });
  }
  return dispatch({
    type: NOTIFICATION,
    payload: {
      notificationMessage: name,
      description: message,
      typeNotification: 'danger'
    }
  });
};
