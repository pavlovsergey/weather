import { SET_MAP_MARKER } from '$store/constants/mapMarker';
import { handlerError } from '../error';
import { Location } from 'expo';
import { API_KEY } from '$config/API_WEATHER/credential';
import axios from 'axios';

export const setMarker = coordinate => {
  return async dispatch => {
    try {
      const address = await Location.reverseGeocodeAsync(coordinate);

      if (address.length === 0) return;
      const { data } = await axios.get(
        `http://api.weatherbit.io/v2.0/current?lat=${coordinate.latitude}&lon=${
          coordinate.longitude
        }&key=${API_KEY}`
      );

      return dispatch({
        type: SET_MAP_MARKER,
        payload: { data: { ...data.data[0], address: address[0] } }
      });
    } catch (e) {
      handlerError(dispatch, e);
    }
  };
};
