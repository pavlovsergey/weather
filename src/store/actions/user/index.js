import { GET_POSITION_USER } from '$store/constants/user';
import { Location } from 'expo';
import { NOTIFICATION } from '$store/constants/notification';

export const getCurrentPosition = () => {
  return async dispatch => {
    try {
      let location = await Location.getCurrentPositionAsync({
        enableHighAccuracy: true,
        maximumAge: 1500
      });
      return dispatch({
        type: GET_POSITION_USER,
        payload: { data: location }
      });
    } catch (e) {
      dispatch({
        type: NOTIFICATION,
        payload: {
          notificationMessage: 'Your position is not detected.',
          description: 'Please turn on geodata in status bar',
          typeNotification: 'info'
        }
      });
    }
  };
};
