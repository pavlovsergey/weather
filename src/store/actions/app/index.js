import { handlerError } from '../error';
import { Asset, Font } from 'expo';
import { STATUS_APP } from '$store/constants/app';

export const initApp = () => {
  return async dispatch => {
    try {
      await cacheAssetsAsync({
        fonts: [
          {
            'Dancing-Script': require('$assets/fonts/Dancing-Script.ttf')
          }
        ]
      });
      return dispatch({ type: STATUS_APP });
    } catch (e) {
      handlerError(dispatch, e);
      dispatch({ type: STATUS_APP });
    }
  };
};

const cacheAssetsAsync = ({ images = [], fonts = [], videos = [] }) => {
  return Promise.all([
    ...cacheImages(images),
    ...cacheFonts(fonts),
    ...cacheVideos(videos)
  ]);
};

function cacheImages(images) {
  return images.map(image => Asset.fromModule(image).downloadAsync());
}

function cacheVideos(videos) {
  return videos.map(video => Asset.fromModule(video).downloadAsync());
}

function cacheFonts(fonts) {
  return fonts.map(font => Font.loadAsync(font));
}
