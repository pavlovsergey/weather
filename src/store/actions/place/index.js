import {
  SET_OPEN_PLACE,
  FAIL_LOAD_WEEK_WEATHER,
  LOADING_WEEK_WEATHER,
  LOAD_WEEK_WEATHER
} from '$store/constants/place';
import { NOTIFICATION } from '$store/constants/notification';
import { handlerError } from '../error';
import { Location } from 'expo';
import { API_KEY } from '$config/API_WEATHER/credential';
import { clearOldDate } from '$utils/date';
import axios from 'axios';

export const setOpenPlace = coordinate => {
  return {
    type: SET_OPEN_PLACE,
    payload: coordinate
  };
};

export const loadWeekWeather = coordinate => {
  return async dispatch => {
    dispatch({
      type: LOADING_WEEK_WEATHER
    });
    try {
      const address = await Location.reverseGeocodeAsync(coordinate);
      const { data } = await axios.get(
        `http://api.weatherbit.io/v2.0/forecast/daily?lat=${
          coordinate.latitude
        }&lon=${coordinate.longitude}&cnt=7&key=${API_KEY}&days=16`
      );
      const weather = clearOldDate(data.data, 7);

      return dispatch({
        type: LOAD_WEEK_WEATHER,
        payload: { weather, address: address[0] }
      });
    } catch (e) {
      handlerError(dispatch, e, FAIL_LOAD_WEEK_WEATHER);
    }
  };
};

export const setPlace = address => {
  return async dispatch => {
    try {
      dispatch({
        type: LOADING_WEEK_WEATHER
      });
      const coordinate = await Location.geocodeAsync(address);
      if (coordinate.length === 0) {
        dispatch({
          type: LOAD_WEEK_WEATHER
        });

        return dispatch({
          type: NOTIFICATION,
          payload: {
            notificationMessage: 'Sorry, place dont found',
            description: 'Try to change the query.',
            typeNotification: 'info'
          }
        });
      }
      dispatch(setOpenPlace(coordinate[0]));
      return dispatch(loadWeekWeather(coordinate[0]));
    } catch (e) {
      handlerError(dispatch, e, FAIL_LOAD_WEEK_WEATHER);
    }
  };
};
