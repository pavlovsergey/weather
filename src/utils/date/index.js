import { month } from './constants';
export const transformDate = created_at => {
  const date = new Date(created_at);
  return `${month[date.getMonth()]} ${date.getDate()}, ${date.getFullYear()}`;
};

export const clearOldDate = (arr, count) => {
  const dateNow = Date.now();
  const newArr = arr.filter(({ valid_date }) => {
    const date = Date.parse(valid_date);
    if (dateNow > date + 86400000) return false;
    return true;
  });

  return newArr.slice(0, count);
};
