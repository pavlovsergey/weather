import React from 'react';
import IconSunny from '$assets/icon/sunny.svg';
import IconSnowflake from '$assets/icon/snowflake.svg';

export const addSymbolTemp = temp => {
  if (temp > 0) {
    return {
      temp: `+${Math.round(temp)}`,
      icon: <IconSunny fill="#fca014" width={25} height={25} />
    };
  }

  return {
    temp: `${Math.round(temp)}`,
    icon: <IconSnowflake fill="#2EA5E1" width={25} height={25} />
  };
};
