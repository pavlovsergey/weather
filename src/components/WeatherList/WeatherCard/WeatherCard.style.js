import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#85CAFF',
    padding: 10,
    margin: 5,
    elevation: 3,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderRadius: 10
  },
  day: {
    color: '#fff',
    fontSize: 16
  },
  contentContainer: {
    flex: 1,
    width: '100%',
    paddingLeft: 5,
    justifyContent: 'flex-end',
    flexDirection: 'row'
  },
  content: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexDirection: 'row',
    paddingRight: 10
  },
  contentToltip: {
    color: '#fff',
    fontSize: 16,
    fontWeight: '600'
  },
  temp: {
    color: '#fff',
    fontSize: 8,
    fontWeight: '600'
  }
});
