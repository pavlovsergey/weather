import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text } from 'react-native';
import { transformDate } from '$utils/date';
import { addSymbolTemp } from '$utils/weather';
import { styles } from './WeatherCard.style';

class WeatherCard extends Component {
  static propTypes = {
    valid_date: PropTypes.string.isRequired,
    temp: PropTypes.number.isRequired
  };

  render() {
    const { temp: tm, valid_date } = this.props;
    const { temp, icon } = addSymbolTemp(tm);
    return (
      <View style={styles.container}>
        <Text style={styles.day}>{transformDate(valid_date)}</Text>
        <View style={styles.contentContainer}>
          <View style={styles.content}>
            <Text style={styles.contentToltip}>{temp}</Text>
            <Text style={styles.temp}>o</Text>
          </View>
          <View style={styles.iconContainer}>{icon}</View>
        </View>
      </View>
    );
  }
}

export default WeatherCard;
