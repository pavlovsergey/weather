import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, FlatList } from 'react-native';
import WeatherCard from './WeatherCard';
import { connect } from 'react-redux';
import { styles } from './WeatherList.style';

const mapStateToProps = ({ place }) => {
  return {
    weather: place.weather
  };
};

@connect(mapStateToProps)
class WeatherList extends Component {
  static propTypes = {
    weather: PropTypes.array.isRequired
  };
  renderDay = ({ item, index }) => {
    return <WeatherCard {...item} />;
  };

  getItemLayoutList = (data, index) => ({
    length: 50,
    offset: this.length * index,
    index
  });

  keyExtractor = (item, index) => `${item.ts} + ${index}`;
  render() {
    const { weather } = this.props;

    return (
      <View style={styles.container}>
        <FlatList
          data={weather}
          renderItem={this.renderDay}
          removeClippedSubviews={true}
          initialNumToRender={10}
          updateCellsBatchingPeriod={25}
          windowSize={10}
          keyExtractor={this.keyExtractor}
          getItemLayout={this.getItemLayoutList}
        />
      </View>
    );
  }
}

export default WeatherList;
