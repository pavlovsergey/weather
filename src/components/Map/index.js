import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { mapStyle } from './config';
import { MapView } from 'expo';
import { Dimensions } from 'react-native';
import MapMarker from '$components/MapMarker';
import { setMarker } from '$store/actions/map';
import { getCurrentPosition } from '$store/actions/user';
import { getPermissions } from '$store/actions/permissions';
import { PERMISSIONS } from '$store/constants/permissions';
import { connect } from 'react-redux';
import { styles } from './Map.style';

const { width } = Dimensions.get('window');

const mapStateToProps = ({ permissions, user, mapMarker }) => {
  return {
    isloacationPermissions: permissions.location,
    userPosition: user.location.coords,
    isRefreshPosition: user.location.isRefreshPosition,
    dataMarker: mapMarker
  };
};

const mapDispatchToProps = { setMarker, getPermissions, getCurrentPosition };
@connect(
  mapStateToProps,
  mapDispatchToProps
)
class Map extends Component {
  static propTypes = {
    isloacationPermissions: PropTypes.bool.isRequired,
    getCurrentPosition: PropTypes.func.isRequired,
    getPermissions: PropTypes.func.isRequired,
    setMarker: PropTypes.func.isRequired,
    userPosition: PropTypes.object.isRequired,
    isRefreshPosition: PropTypes.bool.isRequired,
    dataMarker: PropTypes.array.isRequired
  };
  state = {
    isRefresh: false,
    width: width
  };
  handleSetMarker = ({ nativeEvent: { coordinate } }) => {
    this.props.setMarker(coordinate);
  };

  componentDidUpdate() {
    const {
      getCurrentPosition,
      isloacationPermissions,
      isRefreshPosition
    } = this.props;

    const { isRefresh } = this.state;

    if (isloacationPermissions && !isRefreshPosition) {
      return getCurrentPosition();
    }

    if (!isRefresh && isRefreshPosition) {
      this.setState({ isRefresh: true, width: width - 0.1 });
    }
  }

  handleMapReady = () => {
    this.setState({ width: width - 1 });
  };
  componentDidMount() {
    const {
      getPermissions,
      getCurrentPosition,
      isloacationPermissions,
      isRefreshPosition
    } = this.props;

    if (!isloacationPermissions) {
      return getPermissions(PERMISSIONS.LOCATION);
    }

    if (isloacationPermissions && !isRefreshPosition) {
      getCurrentPosition();
    }
  }

  render() {
    const { userPosition, dataMarker, isRefreshPosition } = this.props;
    const { isRefresh } = this.state;
    const props =
      !isRefresh && isRefreshPosition
        ? {
            region: {
              latitude: userPosition.latitude,
              longitude: userPosition.longitude,
              latitudeDelta: 4,
              longitudeDelta: 4
            }
          }
        : {};

    return (
      <MapView
        onMapReady={this.handleMapReady}
        onLongPress={this.handleSetMarker}
        style={[styles.container, { width: this.state.width }]}
        loadingEnabled={true}
        loadingIndicatorColor="#fca014"
        provider={MapView.PROVIDER_GOOGLE}
        customMapStyle={mapStyle}
        showsCompass={true}
        showsMyLocationButton={true}
        showsUserLocation={true}
        zoomEnabled={true}
        zoomControlEnabled={true}
        mapPadding={styles.padding}
        initialRegion={{
          latitude: userPosition.latitude,
          longitude: userPosition.longitude,
          latitudeDelta: 8,
          longitudeDelta: 8
        }}
        {...props}
      >
        {dataMarker &&
          dataMarker.map((data, index) => {
            return <MapMarker key={index} {...data} />;
          })}
      </MapView>
    );
  }
}
export default Map;
