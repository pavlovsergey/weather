import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  padding: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 40
  }
});
