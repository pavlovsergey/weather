import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  callout: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    padding: 5
  },
  headerToltipContainer: {
    paddingBottom: 5
  },
  headerToltip: {
    fontSize: 18
  },

  contentContainer: {
    width: '100%',

    paddingLeft: 5,
    justifyContent: 'space-around',
    flexDirection: 'row'
  },
  content: {
    paddingLeft: 5,
    justifyContent: 'flex-start',
    flexDirection: 'row'
  },
  contentToltip: {
    fontSize: 18,
    fontWeight: '600'
  },
  temp: {
    fontSize: 10,
    fontWeight: '600'
  }
});
