import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { MapView } from 'expo';
import { Text, View } from 'react-native';
import { addSymbolTemp } from '$utils/weather';
import { WeekWeather } from '$navigator/AppStack/constants';
import { setOpenPlace } from '$store/actions/place';
import { withNavigation } from 'react-navigation';
import { connect } from 'react-redux';
import { styles } from './MapMarker.style';

const mapDispatchToProps = { setOpenPlace };
@withNavigation
@connect(
  null,
  mapDispatchToProps
)
class MapMarker extends Component {
  static propTypes = {
    lon: PropTypes.number.isRequired,
    lat: PropTypes.number.isRequired,
    address: PropTypes.object.isRequired,
    temp: PropTypes.number.isRequired,
    navigation: PropTypes.object.isRequired
  };

  handleChooseCity = () => {
    const { navigation, setOpenPlace, lat, lon } = this.props;

    setOpenPlace({
      latitude: lat,
      longitude: lon
    });
    navigation.navigate(WeekWeather);
  };

  render() {
    const { lat, lon, address, temp: tp } = this.props;
    const place = address.city || address.region;
    const { temp, icon } = addSymbolTemp(tp);
    return (
      <MapView.Marker
        coordinate={{
          latitude: lat,
          longitude: lon
        }}
      >
        <MapView.Callout onPress={this.handleChooseCity}>
          <View style={styles.callout}>
            <View style={styles.headerToltipContainer}>
              <Text>
                <Text style={styles.headerToltip}>{`${place},  `}</Text>
                <Text style={styles.headerToltip}>{address.country}</Text>
              </Text>
            </View>
            <View style={styles.contentContainer}>
              <View style={styles.content}>
                <Text style={styles.contentToltip}>{temp}</Text>
                <Text style={styles.temp}>o</Text>
              </View>
              {icon}
            </View>
          </View>
        </MapView.Callout>
      </MapView.Marker>
    );
  }
}

export default MapMarker;
