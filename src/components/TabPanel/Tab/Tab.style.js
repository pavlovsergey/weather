import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#85CAFF',
    height: 30,
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  border: { borderColor: '#fff', borderLeftWidth: 1 },
  disabledContainer: {
    flex: 1,
    backgroundColor: '#63AEEE',
    height: 30,
    justifyContent: 'center',
    alignItems: 'center'
  },
  name: {
    fontSize: 18,
    color: '#fff',
    textAlign: 'center'
  },
  touch: {
    flex: 1,
    width: '100%'
  }
});
