import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity } from 'react-native';
import { withNavigation } from 'react-navigation';
import { styles } from './Tab.style';

@withNavigation
class Tab extends Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    isActive: PropTypes.bool.isRequired,
    isBorder: PropTypes.bool.isRequired,
    route: PropTypes.string.isRequired,
    navigation: PropTypes.object.isRequired
  };

  handleTransition = () => {
    const { isActive, route, navigation } = this.props;
    if (!isActive) navigation.navigate(route);
  };
  render() {
    const { isActive, name, isBorder } = this.props;
    const style = isActive ? styles.container : styles.disabledContainer;

    return (
      <TouchableOpacity
        onPress={this.handleTransition}
        style={[style, isBorder ? styles.border : {}]}
      >
        <Text style={styles.name}>{name} </Text>
      </TouchableOpacity>
    );
  }
}
export default Tab;
