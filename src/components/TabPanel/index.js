import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import Tab from './Tab';
import { styles } from './TabPanel.style';

export default class TabPanel extends Component {
  static propTypes = {
    activeIndex: PropTypes.number.isRequired,
    arrItem: PropTypes.array.isRequired
  };

  render() {
    const { activeIndex, arrItem } = this.props;

    return (
      <View style={styles.container}>
        {arrItem &&
          arrItem.map((item, index) => {
            return (
              <Tab
                key={index}
                isBorder={index + 1 !== arrItem.length}
                isActive={activeIndex === index}
                {...item}
              />
            );
          })}
      </View>
    );
  }
}
