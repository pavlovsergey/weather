import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    height: 30,
    elevation: 3,
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
});
