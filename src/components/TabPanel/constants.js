import { WeekWeather, Dashboard } from '$navigator/AppStack/constants';
export const arrTab = [
  {
    name: 'Map',
    route: Dashboard
  },
  {
    name: 'Search',
    route: WeekWeather
  }
];
