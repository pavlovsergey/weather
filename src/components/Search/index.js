import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, Image } from 'react-native';
import { Input } from 'react-native-elements';
import { setPlace } from '$store/actions/place';
import { connect } from 'react-redux';
import { styles } from './Search.style';

const mapStateToProps = ({ place }) => {
  return {
    address: place.address
  };
};

const mapDispatchToProps = { setPlace };
@connect(
  mapStateToProps,
  mapDispatchToProps
)
export default class Search extends Component {
  static propTypes = {
    address: PropTypes.object.isRequired,
    setPlace: PropTypes.func.isRequired
  };

  state = {
    value: '',
    isRefresh: true
  };
  handleSetPlace = () => {
    const { value } = this.state;
    const { setPlace } = this.props;
    setPlace(value);
  };
  handleChangeField = ({ nativeEvent: { text } }) => {
    this.setState({ value: text });
  };

  refresh = () => {
    const { address } = this.props;
    const { isRefresh } = this.state;
    const place = address.city || address.region || address.name;
    const value = `${place}, ${address.country}`;
    if (isRefresh && place) {
      this.setState({ value, isRefresh: false });
    }
  };
  componentDidUpdate() {
    this.refresh();
  }
  componentDidMount() {
    this.refresh();
  }
  render() {
    const { value } = this.state;

    return (
      <View style={styles.container}>
        <Input
          placeholder={'Enter place'}
          value={value}
          onChange={this.handleChangeField}
          leftIconContainerStyle={styles.iconContainerStyle}
          inputContainerStyle={styles.inputContainer}
          inputStyle={styles.inputStyle}
          rightIcon={
            <TouchableOpacity onPress={this.handleSetPlace}>
              <Image source={require('$assets/icon/search.png')} />
            </TouchableOpacity>
          }
          rightIconContainerStyle={styles.rightIconContainerStyle}
        />
      </View>
    );
  }
}
