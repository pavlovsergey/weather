import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {},
  iconContainerStyle: {
    marginTop: 5,
    marginBottom: 5,
    paddingRight: 2,
    borderColor: '#eeeeee',
    borderRightWidth: 2,
    height: 25
  },

  inputContainer: {
    backgroundColor: '#fff',
    borderRadius: 20,
    borderColor: 'transparent',
    borderWidth: 0,

    elevation: 1
  },
  rightIconContainerStyle: {
    padding: 5,
    paddingRight: 10
  },
  inputStyle: {
    color: '#bbbbbb',
    fontSize: 14,
    backgroundColor: '#fff',
    borderTopRightRadius: 20,
    borderBottomRightRadius: 20,
    paddingLeft: 15,
    height: 40
  }
});
