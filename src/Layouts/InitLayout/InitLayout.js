import React, { Component } from 'react';
import { View } from 'react-native';
import Notification from '$components/Notification';
import InitStack from '$navigator/InitStack';
import { styles } from './InitLayout.style';

class InitLayout extends Component {
  render() {
    return (
      <View style={styles.container}>
        <InitStack />
        <Notification />
      </View>
    );
  }
}

export default InitLayout;
