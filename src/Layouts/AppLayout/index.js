import React, { Component } from 'react';
import { AppStack } from '$navigator/AppStack';
import { View, StyleSheet, Platform, StatusBar } from 'react-native';

class AppLayout extends Component {
  render() {
    return (
      <View style={styles.container}>
        <AppStack />
      </View>
    );
  }
}

export default AppLayout;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight
  }
});
