import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, ActivityIndicator } from 'react-native';
import Search from '$components/Search';
import { arrTab } from '$components/TabPanel/constants';
import TabPanel from '$components/TabPanel';
import WeatherList from '$components/WeatherList';
import { loadWeekWeather } from '$store/actions/place';
import { connect } from 'react-redux';
import { styles } from './WeekWeatherScreen.style';

const mapStateToProps = ({ place, communication }) => {
  return {
    coordinate: place.coordinate,
    isLoad: communication.weekWeather.isLoad,
    isErrorLoad: communication.weekWeather.isErrorLoad,
    isLoading: communication.weekWeather.isLoading
  };
};

const mapDispatchToProps = { loadWeekWeather };
@connect(
  mapStateToProps,
  mapDispatchToProps
)
class WeekWeatherScreen extends Component {
  static propTypes = {
    loadWeekWeather: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    isErrorLoad: PropTypes.bool.isRequired,
    isLoad: PropTypes.bool.isRequired
  };
  componentDidMount() {
    const { loadWeekWeather, coordinate } = this.props;
    loadWeekWeather(coordinate);
  }
  render() {
    const { isLoad, isLoading, isErrorLoad } = this.props;

    if (isErrorLoad) return <View />;

    if (isLoading || (!isErrorLoad && !isLoad)) {
      return (
        <View style={styles.loadingContainer}>
          <ActivityIndicator size={60} color="#fca014" />
        </View>
      );
    }

    if (isLoad) {
      return (
        <View style={styles.container}>
          <Search />
          <WeatherList />
          <TabPanel activeIndex={1} arrItem={arrTab} />
        </View>
      );
    }
  }
}

export default WeekWeatherScreen;
