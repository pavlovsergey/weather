import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',

    width: '100%',
    height: '100%'
  },
  headerContainer: {
    alignItems: 'center',
    flexWrap: 'wrap',
    position: 'absolute',
    width: '100%',
    top: 60
  },
  textContainer: {
    textAlign: 'center'
  },
  hello: {
    fontSize: 22,
    color: '#fff',
    fontWeight: '500'
  },
  brand: {
    fontSize: 22,
    color: '#fca014',
    fontWeight: '600'
  },
  name: {
    fontSize: 22,
    color: '#fff',
    fontWeight: '500'
  }
});
