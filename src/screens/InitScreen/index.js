import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ActivityIndicator, View, Text } from 'react-native';
import { App } from '$navigator/InitStack/constants';
import { initApp } from '$store/actions/app';
import { connect } from 'react-redux';
import { styles } from './InitScreen.style';

const mapStateToProps = state => {
  return {
    status: state.app.status
  };
};

const mapDispatchToProps = { initApp };

@connect(
  mapStateToProps,
  mapDispatchToProps
)
class InitScreen extends Component {
  static propTypes = {
    status: PropTypes.string.isRequired,
    navigation: PropTypes.object.isRequired,
    initApp: PropTypes.func.isRequired
  };

  componentDidUpdate() {
    const { status, navigation } = this.props;
    if (status === App) return navigation.navigate(App);
  }

  componentDidMount() {
    const { initApp, navigation } = this.props;
    initApp();
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <Text style={styles.textContainer}>
            <Text style={styles.hello}> Hello! </Text>
          </Text>
          <Text style={styles.name}> This is your weather </Text>
        </View>
        <ActivityIndicator size={60} color="#fca014" />
      </View>
    );
  }
}

export default InitScreen;
