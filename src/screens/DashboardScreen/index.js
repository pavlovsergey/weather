import React, { Component } from 'react';
import { View } from 'react-native';
import Map from '$components/Map';
import TabPanel from '$components/TabPanel';
import { arrTab } from '$components/TabPanel/constants';
import { styles } from './DashboardScreen.style';

class DashboardScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Map />
        <View style={styles.tabContainer}>
          <TabPanel activeIndex={0} arrItem={arrTab} />
        </View>
      </View>
    );
  }
}

export default DashboardScreen;
