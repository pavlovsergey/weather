import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  tabContainer: {
    zIndex: 2,
    width: '100%',
    position: 'absolute',
    bottom: 0,
    right: 0
  }
});
