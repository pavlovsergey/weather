module.exports = function(api) {
  api.cache(true);
  return {
    presets: ['babel-preset-expo'],
    env: {
      development: {
        plugins: ['@babel/transform-react-jsx-source']
      }
    },

    plugins: [
      [
        'module-resolver',
        {
          alias: {
            $utils: './src/utils',
            $config: './src/config',
            $assets: './assets',
            $screens: './src/screens',
            $shared: './src/shared',
            $components: './src/components',
            $store: './src/store',
            $navigator: './src/navigator',
            $Layouts: './src/Layouts'
          }
        }
      ]
    ]
  };
};
